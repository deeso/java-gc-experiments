package edu.rice.cs.seclab.dso;

import java.lang.instrument.Instrumentation;
import java.net.URISyntaxException;

public class ObjectSizeFetcher {
    private static Instrumentation instrumentation;

    public static void premain(String args, Instrumentation inst) {
        instrumentation = inst;
    }

    public static long getObjectSize(Object o) {
    	if (instrumentation == null) return 0; 
        return instrumentation.getObjectSize(o);
    }
    
    public static void agentmain(String args, Instrumentation inst) throws Exception {
        instrumentation = inst;
     }

    /**
     * Programmatic hook to dynamically load javaagent at runtime.
     */
    public static void initialize() {
        if (instrumentation == null) {
            try {
				ObjectSizeFetcherLoader.loadAgent();
			} catch (URISyntaxException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
    }
}