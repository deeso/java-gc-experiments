package edu.rice.cs.seclab.dso;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.Principal;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.util.HashMap;

import javax.net.ssl.HandshakeCompletedListener;
import javax.net.ssl.SSLPeerUnverifiedException;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSessionContext;
import javax.net.ssl.SSLSocket;
import javax.security.cert.X509Certificate;

import org.bouncycastle.crypto.tls.TlsClient;
import org.bouncycastle.crypto.tls.TlsClientProtocol;
import org.bouncycastle.crypto.tls.TlsProtocol;
import org.bouncycastle.crypto.tls.TlsSession;


public class BouncyCastleSSLSocket extends SSLSocket implements SSLSession {
	HashMap<String, Object> values = new HashMap<String, Object>();
	private Socket mySocket;
	private MyTlsClient myClient;
	static private SecureRandom mySecureRandom = new SecureRandom();
	private TlsProtocol myProtocol;
	private TlsSession mySession;
	private int savedCipherKeySize;
	
	
	
	public BouncyCastleSSLSocket(InetSocketAddress addr, InetSocketAddress laddr, int connectTimeout) throws IOException {
		connectTLS(addr, laddr, connectTimeout);
	}
	
	public BouncyCastleSSLSocket(String remoteHost, int remotePort, int connectTimeout) throws IOException {
		connectTLS(remoteHost, remotePort, connectTimeout);
	}
	public BouncyCastleSSLSocket(String remoteHost, int remotePort) throws IOException {
		connectTLS(remoteHost, remotePort);
	}
	
	private void connectTLS(String remoteHost, int remotePort,
			int connectTimeout) throws IOException {
		InetSocketAddress remoteAddr = new InetSocketAddress(remoteHost, remotePort);
		connectTLS(remoteAddr, null, connectTimeout);
	}

	public TlsClientProtocol openTlsConnection(Socket s, TlsClient client) throws IOException
    {
		TlsClientProtocol protocol = new TlsClientProtocol(s.getInputStream(), s.getOutputStream(), mySecureRandom );
        protocol.connect(client);
        return protocol;
    }
	
	public void connectTLS(String remoteHost, int port) throws IOException {
		connectTLS(remoteHost, port, 0);
	}
	
	public void connectTLS(InetSocketAddress addr, InetSocketAddress laddr, int connectTimeout) throws IOException {
		mySocket = new Socket(addr.getAddress(), addr.getPort());
		mySocket.setSoTimeout(connectTimeout);
		if (laddr != null)
			mySocket.bind(laddr);
        myClient = new MyTlsClient(null);
        myProtocol = openTlsConnection(mySocket, myClient);
        mySession = myClient.getSessionToResume();
        saveKeyInfo();
	}
	
	@Override
	public void addHandshakeCompletedListener(
			HandshakeCompletedListener listener) {
		
	}

	@Override
	public boolean getEnableSessionCreation() {
		return true;
	}

	@Override
	public String[] getEnabledCipherSuites() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String[] getEnabledProtocols() {
		return null;
	}

	@Override
	public boolean getNeedClientAuth() {
		// 
		return false;
	}

	@Override
	public SSLSession getSession() {
		return (SSLSession) this;
	}

	@Override
	public String[] getSupportedCipherSuites() { return null;}

	@Override
	public String[] getSupportedProtocols() {return null; }

	@Override
	public boolean getUseClientMode() {return false; }

	@Override
	public boolean getWantClientAuth() {return false;}

	@Override
	public void removeHandshakeCompletedListener(
			HandshakeCompletedListener listener) {}

	@Override
	public void setEnableSessionCreation(boolean flag) {}

	@Override
	public void setEnabledCipherSuites(String[] suites) {}

	@Override
	public void setEnabledProtocols(String[] protocols) {}

	@Override
	public void setNeedClientAuth(boolean need) {}

	@Override
	public void setUseClientMode(boolean mode) {}

	@Override
	public void setWantClientAuth(boolean want) {}

	@Override
	public void startHandshake() throws IOException {}
	
	public void close() throws IOException{
		myProtocol.close();
	}
	
	@Override
	public InputStream getInputStream() throws IOException {
		return myProtocol.getInputStream();
	}
	
	@Override
	public OutputStream getOutputStream() throws IOException {
		return myProtocol.getOutputStream();
	}

	
	public int getApplicationBufferSize() {
		return 0;
	}

	
	/*public int getCipherSuiteInt() {
		return myProtocol.getCipherSuite();
	}*/

	
	public long getCreationTime() {
		return 0;
	}

	
	public byte[] getId() {
		return mySession.getSessionID();
	}

	
	public long getLastAccessedTime() {
		return 0;
	}

	
	public Certificate[] getLocalCertificates() {
		return null;
	}

	
	public Principal getLocalPrincipal() {
		return null;
	}

	
	public int getPacketBufferSize() {
		return 0;
	}

	
	public X509Certificate[] getPeerCertificateChain()
			throws SSLPeerUnverifiedException {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Certificate[] getPeerCertificates()
			throws SSLPeerUnverifiedException {
		return null;
	}

	
	public String getPeerHost() {
		return mySocket.getRemoteSocketAddress().toString();
	}

	
	public int getPeerPort() {
		// TODO Auto-generated method stub
		return 0;
	}

	
	public Principal getPeerPrincipal() throws SSLPeerUnverifiedException {
		return null;
	}

	
	public String getProtocol() {
		return new String("TLS");
	}

	
	public SSLSessionContext getSessionContext() {
		// TODO Auto-generated method stub
		return null;
	}

	
	public Object getValue(String name) {
		if (values.containsKey(name)) {
			return values.get(name);
		}
		return null;
	}

	
	public String[] getValueNames() {
		// TODO Auto-generated method stub
		String [] themvalues = new String[values.size()];
		int pos = 0;
		for (String v : values.keySet()) {
			themvalues[pos] = v;
			pos++;
		}
		return themvalues;
	}

	
	public void invalidate() {}

	
	public boolean isValid() {return true;}

	
	public void putValue(String name, Object value) {
		values.put(name, value);
	}

	public void saveKeyInfo() {
		//this.savedKeyBlock = this.myProtocol.getKeyBlock();
		//this.savedCipherKeySize = this.myProtocol.getCipherKeySize();
		//bu_seed = this.myProtocol.getBUSeed();
		//bu_masterKey = this.myProtocol.getBUMasterKey();
	}
	
	public void removeValue(String name) {
		if (values.containsKey(name)) {
			values.remove(name);
		}
	}
	public TlsProtocol getTlsProtocol() {
		return myProtocol;
	}
    /*public byte [] getKeyBlock() {
        return this.myProtocol.getKeyBlock();
    }*/
    
    public int getCipherKeySize() {
    	return this.savedCipherKeySize; 
    }

	/*public byte[] getSeed() {
		return this.myProtocol.getBUSeed();
	}

	public byte[] getMasterKey() {
		return this.myProtocol.getBUMasterSecret();
	}*/
	
	public String getTlsCipherTypeString( ) {
		return null;
	}

	public String getCipherSuite() {
		// TODO Auto-generated method stub
		return null;
	}
}
