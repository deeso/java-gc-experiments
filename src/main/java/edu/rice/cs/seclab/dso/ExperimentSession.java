package edu.rice.cs.seclab.dso;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.security.auth.Destroyable;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.cookie.BasicClientCookie;
import org.apache.http.message.BasicNameValuePair;

public class ExperimentSession {
	static public String GET_LINE = "GET %s HTTP/1.1\r\n";
	static public String POST_LINE = "POST %s HTTP/1.1\r\n";
	static public String HOST_LINE = "Host: %s\r\n";
	static public String COOKIE_LINE = "Cookie: %s:%s\r\n";
	static public String CONTENT_LENGTH = "Content-Length: %d\r\n";
	static public String CONNECTION_CLOSE = "Connection: close\r\n";
	static public String POST_PARAMS = "username=%s&password=%s\r\n";
	static public String CRLF = "\r\n";

	@SuppressWarnings("deprecation")
	RequestConfig defaultRequestConfig = RequestConfig.custom()
			.setCookieSpec(CookieSpecs.BEST_MATCH)
			.setExpectContinueEnabled(true)
			.setStaleConnectionCheckEnabled(true).setSocketTimeout(120 * 1000)
			.setConnectTimeout(5000).setConnectionRequestTimeout(120 * 1000)
			.build();
	volatile Boolean maxed = false;
	Boolean reportedMaxedOut = false;
	private static final int BLOCK_SIZE = 512 * 4 * 8;
	private static final String SESSIONID_NAME = "THESESSIONID";
	private static final long THREAD_SLEEP_TIME = 750L;
	private static final int MAX_RANDOM_STRINGS = 100;
	private static final int MAX_RANDOM_CHARS = 100;
	private static int NUM_RANDOM_CHARS = MAX_RANDOM_CHARS;

	public class DestroyableString implements Destroyable {
		public String theString = null;

		public DestroyableString() {
			theString = new String();
		}

		public DestroyableString(String string) {
			theString = string;
		}

		@Override
		public void destroy() {
			theString = null;
		}

		@Override
		public boolean isDestroyed() {
			return theString == null;
		}

		public String toString() {
			return theString;
		}

		public int length() {
			return theString.length();
		}

		public byte[] getBytes() {
			return theString.getBytes();
		}
		
		public void append(String d) {
			theString = theString + d;
		}

		public String substring(int i, int j) {
			if (i < 0)
				i = 0;
			if (j < i)
				j = i + 1;
			if (j > theString.length())
				j = theString.length();
			if (i == j)
				return theString;
			return theString.substring(i, j);
		}
	}

	// public String expID =
	// ExperimentParameterGeneration.getinstance().nextUserId();
	public DestroyableString user = new DestroyableString(
			ExperimentParameterGeneration.getinstance().nextUserId());
	public DestroyableString pass = new DestroyableString(
			ExperimentParameterGeneration.getinstance().nextPasswordId());
	public String host = null;
	//HttpClient client = null;
	BasicCookieStore cookieStore = new BasicCookieStore();
	// experiment parameters
	long sessLifetime = 0;
	long memConsume = 0;
	Boolean continueActivity = new Boolean(true);
	Boolean alive = new Boolean(false);
	volatile Boolean shutdown = new Boolean(false);
	Boolean error = new Boolean(false);
	static Long MIN_SIZE = null;// ObjectSizeFetcher.getObjectSize(this);
	long MAX_SIZE = ObjectSizeFetcher.getObjectSize(this);
	Boolean completed = new Boolean(false);
	long timerSpacing = 0;
	long memConsumeSteps = 0;

	Integer completedSteps = 0;
	Integer remainingSteps = 0;
	Thread dataGramSender = null;
	// set the user session id
	public final String LOGIN_URL = "/login.html";
	public final String MED_URL = "/htmlfile_med.html";
	public final String SMALL_URL = "/htmlfile_small.html";
	Timer nextStepTimer = null;
	DestroyableString data = null;
	private long perReqConsume;
	private long sessionStartTime = 0;
	Boolean usingBouncyCastle = false;
	private DestroyableString sessionId = null;
	private boolean wipe = false;
	private long allowedRequests;
	private boolean expired;
	public boolean use_ssl_socket = false;
	private boolean set_ssl_socket_null = false;
	private boolean pause;

	private void cancelTimer() {
		try {
			nextStepTimer.cancel();
			nextStepTimer.purge();
		} catch (Exception ex) {

		}
	}
	
	
	public void setPause() {
		this.pause = false ? this.pause : true;
	}

	public ExperimentSession(JavaClientExperiments jclientExp) {
		this.allowedRequests = jclientExp.getAllowedRequests();
		this.memConsume = jclientExp.getMemoryPerSession();
		this.sessLifetime = jclientExp.getSessLifetime();
		NUM_RANDOM_CHARS = (int) (MAX_RANDOM_CHARS * jclientExp
				.getMemoryPressureFactor());
		if (this.allowedRequests == 0)
			this.allowedRequests = 100;
		this.timerSpacing = (this.sessLifetime / this.allowedRequests) * 1000;
		this.perReqConsume = this.memConsume / this.allowedRequests;
		this.remainingSteps = (int) this.allowedRequests;
		// this.jclientExp = jclientExp;
		this.host = jclientExp.getRemoteHost();
		data = new DestroyableString();
		sessionId = new DestroyableString(ExperimentParameterGeneration
				.getinstance().nextSessionId());
		addCookie(SESSIONID_NAME, sessionId.toString(), "." + host, "/");
		usingBouncyCastle = jclientExp.useBouncyCastle();
		buildClient();
		wipe = jclientExp.isDestroy_pass();
	}

	public void start() {
		synchronized (alive) {
			sessionStartTime = System.currentTimeMillis();
			remainingSteps--;
			alive = true;
			completed = false;
			nextStepTimer = new Timer();
			nextStepTimer.schedule(new PerformLogin(), timerSpacing);
		}
		if (MIN_SIZE == null)
			MIN_SIZE = ObjectSizeFetcher.getObjectSize(this);
	}

	// public long calculateSize() {
	// if (data.size() > 0)
	// return ObjectSizeFetcher.getObjectSize(this)
	// + ObjectSizeFetcher.getObjectSize(data)
	// + ObjectSizeFetcher.getObjectSize(data.get(0))
	// * data.size();
	// else
	// return ObjectSizeFetcher.getObjectSize(this)
	// + ObjectSizeFetcher.getObjectSize(data)
	// + ObjectSizeFetcher.getObjectSize(0 * data.size());
	// }
	public long calculateSize() {
		long sz = data == null ? MIN_SIZE : data.length() + MIN_SIZE;
		return sz;
	}

	public void schedule_next_task(long milli) {
		synchronized (alive) {
			remainingSteps--;

			// this.jclientExp.stdout(String.format("Executed %d of %d steps, completed work",
			// completedSteps, totRequests));
			// this.jclientExp.stdout(String.format("Consumed %dK (ideal = %dK) bytes of %dK memory",
			// getExpectedMemConsumption()/(1024*8), getDataSize()/(1024*8),
			// memConsume/(1024*8)));

			if (shutdown || expired /* || remainingSteps <= 0 */) {
				alive = false;
				completed = true;
				cancelTimer();
				if (wipe) {
					user.destroy();
					sessionId.destroy();
					pass.destroy();
				}
				// reportSize();
				JavaClientExperiments.getExp()
						.handleCompleteSession(getExpID());
			}
			if (alive && !error) {
				// reportSize();
				while (pause){
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
					}
				}
				if (ExperimentParameterGeneration.getinstance().coinFlip()) {
					// this.jclientExp.stdout(String.format("Scheduling Medium Request task for %dms",milli));
					nextStepTimer.schedule(new PerformMediumReq(), milli);
				} else {
					// this.jclientExp.stdout(String.format("Scheduling Medium Request task for %dms",milli));
					nextStepTimer.schedule(new PerformSmalleReq(), milli);
				}
			} else if (!completed && error) {
				// this.jclientExp.stdout("Failed to schedule the next task!");
				completed = false;
				alive = false;
				cancelTimer();
				// reportSize();
				JavaClientExperiments.getExp().handleFailedSession(getExpID());
			}
		}
	}

	public void setDataBlock(long memCon) {
		StringBuffer sb = new StringBuffer();
		if (memCon == 0) {
			memCon = 32;
		}

		String d = String.format("%04x_%s_b4ds3cr3tsh3r3", this.completedSteps,
				user.toString());
		//sb.append(data.toString());

		long cnt = memCon / d.length();
		try {
			long d_len = d.length();
			long d_size = data.length()+d_len;
			
			for (; cnt > 0; cnt-- ) {
				sb.append(d);
//				data.append(d);	
				d_size += d_len;
				//if (!JavaClientExperiments.getExp().allowedToAllocate(d_size)) break;
			}
				
		} catch (OutOfMemoryError ex) {
			String out = String.format(
					"Error: %s:%04x failed %04x memory allocation in setDataBlock, "
							+ ex.toString(), user, completedSteps);
			JavaClientExperiments.getExp().stdout(out);
			//sb.setLength(data.length() * 2);
		} catch (Exception e) {
			String out = String.format(
					"XXXX === Error: %s:%04x failed %04x memory allocation in setDataBlock, "
							+ e.toString(), user, completedSteps);
			JavaClientExperiments.getExp().stdout(out);
			
		}

		if (wipe)
			data.destroy();
		data = new DestroyableString(sb.toString());
	}

	public void schedule_next_task() {
		schedule_next_task(timerSpacing);
	}

	public long getDataSize() {
		long size = 0;
		// for (Character [] b : data)
		// size += b.length;
		// return size;
		return data.length();
	}

	private void moreData(long memCon) {
		synchronized (data) {
			if (memCon < BLOCK_SIZE) {
				setDataBlock(BLOCK_SIZE);
			} else {
				setDataBlock(memCon);
			}
		}
	}

	public long getAdditionalPressureSize() {
		long emc = getExpectedMemConsumption();
		long should_incr = perReqConsume;
		double max = JavaClientExperiments.getExp().getMemoryPressureFactor(), min = max - .2;
		if (max < .2)
			min = 0.0;

		float fudge = (float) ExperimentParameterGeneration.getinstance()
				.randomPercent(min, max);
		/*
		 * if (getDataSize() > emc &&
		 * ExperimentParameterGeneration.getinstance().coinFlip()) { should_incr
		 * = 0; } else if
		 * (ExperimentParameterGeneration.getinstance().coinFlip()) {
		 * should_incr += (long) (should_incr*fudge); }
		 */
		return should_incr;
	}

	private long getExpectedMemConsumption() {
		return completedSteps * perReqConsume;
	}

	private SSLSocket buildSSLSocket(String host, int port) {
		try {
			SSLSocketFactory sslsf = (SSLSocketFactory) MySSLSocketFactory
					.getDefault();
			SSLSocket sslc = (SSLSocket) sslsf.createSocket(host, port);
			sslc.startHandshake();
			return sslc;
		} catch (UnknownHostException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private HttpClient buildClient() {
		//
		HttpClient client = null;
		javax.net.ssl.SSLContext sslcontext = null;
		if (!usingBouncyCastle) {
			try {
				sslcontext = org.apache.http.ssl.SSLContexts
						.custom()
						.loadTrustMaterial(
								null,
								new org.apache.http.conn.ssl.TrustSelfSignedStrategy())
						.build();
				org.apache.http.conn.ssl.SSLConnectionSocketFactory sslsf = new org.apache.http.conn.ssl.SSLConnectionSocketFactory(
						sslcontext, new String[] { "TLSv1.2" }, null,
						new org.apache.http.conn.ssl.NoopHostnameVerifier());
				HttpClientBuilder httpcb = HttpClientBuilder.create()
						.setSSLSocketFactory(sslsf)
						.setDefaultCookieStore(cookieStore);
				;
				client = httpcb.build();
			} catch (KeyManagementException | NoSuchAlgorithmException
					| KeyStoreException e) {
				e.printStackTrace();
			}
		} else {
			usingBouncyCastle = true;
			MyBasicConnectionManager connMgr = new MyBasicConnectionManager();
			HttpClientBuilder httpcb = HttpClientBuilder.create();
			httpcb.setConnectionManager(connMgr).setDefaultCookieStore(
					cookieStore);
			client = httpcb.build();
		}
		return client;
	}

	private void sendDatagramPacket() {
		if (data == null || data.length() == 0)
			return;
		if (dataGramSender != null && dataGramSender.isAlive())
			return;
		byte[] dg_data;
		try {
			dg_data = data.getBytes();
		} catch (OutOfMemoryError ex) {
			String out = String.format(
					"Error: %s:%04x failed %04x memory allocation in sendDatagramPacket, "
							+ ex.toString(), user, completedSteps);
			JavaClientExperiments.getExp().stdout(out);

			String test = data.substring(0, 32 - 1)
					+ data.substring(data.length() - 64, data.length() - 1);
			dg_data = test.getBytes();
		}
		final byte[] _dg_data = dg_data;
		dataGramSender = new Thread() {
			byte[] dg_data = _dg_data;

			@Override
			public void run() {
				try {
					int t = dg_data.length > 8192 ? 8192 : dg_data.length;
					InetAddress address = InetAddress.getByName("127.0.1.2");
					DatagramPacket dgp = new DatagramPacket(dg_data, t,
							address, 49900);
					DatagramSocket datagramSocket = new DatagramSocket();
					// datagramSocket.send(dgp);
					datagramSocket.close();
				} catch (UnknownHostException e) {
					String out = String.format(
							"Error: %s:%04x failed packet send in sendDatagramPacket, "
									+ e.toString(), user, completedSteps);
					JavaClientExperiments.getExp().stdout(out);

				} catch (IOException e) {
					String out = String.format(
							"Error: %s:%04x failed packet send in sendDatagramPacket, "
									+ e.toString(), user, completedSteps);
					JavaClientExperiments.getExp().stdout(out);
				}

			}
		};
		dataGramSender.start();
	}

	private void login() throws IOException {
		HttpClient client = buildClient();
		long startTime = System.currentTimeMillis();
		HttpPost post = new HttpPost(getLoginUrl());
		RequestConfig requestConfig = RequestConfig.copy(defaultRequestConfig)
				.build();
		post.setConfig(requestConfig);
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("user", user.toString()));
		nameValuePairs.add(new BasicNameValuePair("password", String
				.valueOf(pass)));
		post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		HttpResponse response = client.execute(post);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));
		String line = "";
		StringBuilder op = new StringBuilder();

		while ((line = rd.readLine()) != null) {
			op.append(line);
		}
		printCookieValues();
		sessionId = new DestroyableString(ExperimentParameterGeneration
				.getinstance().nextSessionId());
		addCookie(SESSIONID_NAME, sessionId.toString(), "." + host, "/");
		printCookieValues();
		
		JavaClientExperiments.getExp().logUserAuth(
				String.format("%08x", startTime), user.toString(),
				pass.toString(), sessionId.toString());
		// if (JavaClientExperiments.getExp().isDestroy_pass()) {
		// for (int i = 0; i < pass.length; i++)
		// pass[i] = '\0';
		// }
		completedSteps++;
		JavaClientExperiments.getExp().incrementRequest();
		updateMemoryConsumption();
	}

	private void login2() throws IOException {
		long startTime = System.currentTimeMillis();
		SSLSocket ssl_sock = buildSSLSocket(host, 443);
		//Socket ssl_sock = new Socket(host, 80);
		if (ssl_sock == null) {
			throw new IOException("SSL Socket invalid");
		}
		
		//PrintWriter request = new PrintWriter( ssl_sock.getOutputStream() );
		BufferedWriter w = new BufferedWriter(new OutputStreamWriter(
				ssl_sock.getOutputStream(), "UTF-8"));
		BufferedReader r = new BufferedReader(new InputStreamReader(
				ssl_sock.getInputStream()));

		StringBuilder op = new StringBuilder();
		float pct = ExperimentParameterGeneration.getinstance().randomPercent();
		String random_data = "";
//		random_data = ExperimentParameterGeneration.getinstance()
//				.randomString((int) (pct * NUM_RANDOM_CHARS));
//		long sleep_time = (long) (THREAD_SLEEP_TIME * pct);
		// sleep_time = sleep_time >> 1;
		// try {
		// if (sleep_time == 0)
		// Thread.sleep(100);
		// else
		// Thread.sleep(sleep_time);
		// } catch (InterruptedException e) {
		// }
		StringBuilder rsb = new StringBuilder(); 
		String post_params = String.format(POST_PARAMS,
				URLEncoder.encode(user.toString(), "UTF-8"),
				URLEncoder.encode(pass.toString(), "UTF-8")); 
		rsb.append(String.format(POST_LINE, LOGIN_URL));
        rsb.append(String.format(HOST_LINE, host));
        rsb.append(String.format(COOKIE_LINE, SESSIONID_NAME, sessionId.toString()));
        rsb.append(String.format(CONTENT_LENGTH, post_params.length()));
        rsb.append(String.format(CONNECTION_CLOSE));
        rsb.append(String.format(CRLF));
        rsb.append(post_params);
        rsb.append(String.format(CRLF));
        String req = rsb.toString();
        w.write(req);
        w.flush();
		//w.write(String.format(CRLF));

		String line = "";

		op = new StringBuilder();
		op.append(random_data);
		while ((line = r.readLine()) != null) {
			op.append(line);
		}
		try {
			ssl_sock.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}finally {
			if (set_ssl_socket_null) ssl_sock = null;
		}
		// sleep_time = sleep_time >> 1;
		// try {
		// if (sleep_time == 0)
		// Thread.sleep(100);
		// else
		// Thread.sleep(sleep_time);
		// } catch (InterruptedException e) {
		// }
		printCookieValues();
		sessionId = new DestroyableString(ExperimentParameterGeneration
				.getinstance().nextSessionId());
		addCookie(SESSIONID_NAME, sessionId.toString(), "." + host, "/");
		printCookieValues();
		
		JavaClientExperiments.getExp().logUserAuth(
				String.format("%08x", startTime), user.toString(),
				pass.toString(), sessionId.toString());
		// if (JavaClientExperiments.getExp().isDestroy_pass()) {
		// for (int i = 0; i < pass.length; i++)
		// pass[i] = '\0';
		// }
		completedSteps++;
		JavaClientExperiments.getExp().incrementRequest();
		updateMemoryConsumption();
	}

	private void medium_req2() throws IOException {
		SSLSocket ssl_sock = buildSSLSocket(host, 443);
		if (ssl_sock == null) {
			throw new IOException("SSL Socket invalid");
		}

		BufferedWriter w = new BufferedWriter(new OutputStreamWriter(
				ssl_sock.getOutputStream(), "UTF-8"));
		BufferedReader r = new BufferedReader(new InputStreamReader(
				ssl_sock.getInputStream()));

		StringBuilder op = new StringBuilder();
		float pct = ExperimentParameterGeneration.getinstance().randomPercent();
		String random_data = "";
//		random_data = ExperimentParameterGeneration.getinstance()HttpClient client = buildClient();
//				.randomString((int) (pct * NUM_RANDOM_CHARS));
//		long sleep_time = (long) (THREAD_SLEEP_TIME * pct);
		// sleep_time = sleep_time >> 1;
		// try {
		// if (sleep_time == 0)
		// Thread.sleep(100);
		// else
		// Thread.sleep(sleep_time);
		// } catch (InterruptedException e) {
		// }

		StringBuilder rsb = new StringBuilder(); 
		rsb.append(String.format(GET_LINE, MED_URL));
		rsb.append(String.format(HOST_LINE, host));
		rsb.append(String.format(COOKIE_LINE, SESSIONID_NAME, sessionId.toString()));
        rsb.append(String.format(CONNECTION_CLOSE));
		rsb.append(String.format(CRLF));
        String req = rsb.toString();
        w.write(req);
        w.flush();

		String line = "";

		op = new StringBuilder();
		op.append(random_data);
		while ((line = r.readLine()) != null) {
			op.append(line);
		}
		try {
			ssl_sock.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		} finally {
			if (set_ssl_socket_null) ssl_sock = null;
		}
		// sleep_time = sleep_time >> 1;
		// try {
		// if (sleep_time == 0)
		// Thread.sleep(100);
		// else
		// Thread.sleep(sleep_time);
		// } catch (InterruptedException e) {
		// }

		completedSteps++;
		JavaClientExperiments.getExp().incrementRequest();
		updateMemoryConsumption();
	}

	private void small_req2() throws IOException {
		SSLSocket ssl_sock = buildSSLSocket(host, 443);
		if (ssl_sock == null) {
			throw new IOException("SSL Socket invalid");
		}

		BufferedWriter w = new BufferedWriter(new OutputStreamWriter(
				ssl_sock.getOutputStream(), "UTF-8"));
		BufferedReader r = new BufferedReader(new InputStreamReader(
				ssl_sock.getInputStream()));

		StringBuilder op = new StringBuilder();
		float pct = ExperimentParameterGeneration.getinstance().randomPercent();
		String random_data = "";
//		String random_data = ExperimentParameterGeneration.getinstance()
//				.randomString((int) (pct * NUM_RANDOM_CHARS));
//		long sleep_time = (long) (THREAD_SLEEP_TIME * pct);
		// sleep_time = sleep_time >> 1;
		// try {
		// if (sleep_time == 0)
		// Thread.sleep(100);
		// else
		// Thread.sleep(sleep_time);
		// } catch (InterruptedException e) {
		// }
		StringBuilder rsb = new StringBuilder(); 
		rsb.append(String.format(GET_LINE, SMALL_URL));
		rsb.append(String.format(HOST_LINE, host));
		rsb.append(String.format(COOKIE_LINE, SESSIONID_NAME, sessionId.toString()));
        rsb.append(String.format(CONNECTION_CLOSE));
		rsb.append(String.format(CRLF));
        String req = rsb.toString();
        w.write(req);
        w.flush();

		String line = "";

		op = new StringBuilder();
		op.append(random_data);
		while ((line = r.readLine()) != null) {
			op.append(line);
		}
		//sleep_time = sleep_time >> 1;HttpClient client = buildClient();
		// try {
		// if (sleep_time == 0)
		// Thread.sleep(100);
		// else
		// Thread.sleep(sleep_time);
		// } catch (InterruptedException e) {
		// }
		try {
			ssl_sock.close();
		} catch (IOException ex) {
			ex.printStackTrace();
		}finally {
			if (set_ssl_socket_null) ssl_sock = null;
		}
		completedSteps++;
		JavaClientExperiments.getExp().incrementRequest();
		updateMemoryConsumption();
	}

	public void printCookieValues() {
		// for (Cookie cookie : cookieStore.getCookies()) {
		// this.jclientExp.stdout("Cookie name: "+cookie.getName() +
		// " value: "+cookie.getValue() + "host: "+cookie.getDomain());
		// }
	}

	public void addCookie(String name, String value, String domain, String path) {
		cookieStore.clear();
		BasicClientCookie cookie = new BasicClientCookie(name, value);
		cookie.setDomain(domain);
		cookie.setPath(path);
		cookieStore.addCookie(cookie);
	}

	public long getNextTaskTime() {
		// long current = System.currentTimeMillis() - sessionStartTime;
		// if (current > completedSteps * timerSpacing) {
		// return current - completedSteps * timerSpacing;
		// }
		// float t = (float)
		// (ExperimentParameterGeneration.getinstance().coinFlip() ? 1.0 : -1.0)
		// *
		// ExperimentParameterGeneration.getinstance().randomPercent();
		// float fudge = (float) ((float) timerSpacing * (.3* t));
		// return timerSpacing + (long) fudge;
		return timerSpacing;
	}

	public void medium_req() throws ClientProtocolException, IOException {
		HttpClient client = buildClient();
		HttpGet get = new HttpGet(getMedUrl());
		buildClient();
		HttpResponse response = client.execute(get);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));
		String line = "";
		StringBuilder op = new StringBuilder();

		while ((line = rd.readLine()) != null) {
			op.append(line);
		}
		client = null;
		completedSteps++;
		updateMemoryConsumption();
	}

	public void reportSize() {
		long size = calculateSize();
		long end = System.currentTimeMillis();
		long current = end / 1000 - sessionStartTime / 1000;

		expired = current > sessLifetime;
		if (size > MAX_SIZE)
			MAX_SIZE = size;
		long time = System.currentTimeMillis();
		long smem = Runtime.getRuntime().maxMemory()
				- Runtime.getRuntime().freeMemory();
		int is_completed = this.completed ? 1 : 0, is_maxed = maxed ? 1 : 0, is_expired = this.expired ? 1
				: 0;
		String out = String
				.format("Time:%08x-User:%s-min:%08x-mem:%08x-max:%08x-smem:%08x-maxed:%02x-expired:%02x-completed:%02x-steps:%04x",
						time, user, MIN_SIZE, size, MAX_SIZE, smem, is_maxed,
						is_expired, is_completed, completedSteps);
		JavaClientExperiments.getExp().stdout(out);
	}

	private void updateMemoryConsumption() {
		if (!JavaClientExperiments.getExp().allowedToAllocate(0)) {
			// System.out.println(user+" Unable to allocate data, "+data.length());
			// if (!reportedMaxedOut && data.size() > 0) {
			if (!reportedMaxedOut && data.length() > 0) {
				maxed = true;
				reportedMaxedOut = true;
				// reportSize();
			}
			// if (data.size() > 0) {
			if (data.length() > 10) {
				int num = ExperimentParameterGeneration.getinstance()
						.randomInt(
						// data.size());
								data.length() - 2);
				num = Math.abs(num) > 0 ? Math.abs(num) : 1;

				String t = data.substring(num, data.length() - 1);
				if (wipe)
					data.destroy();
				data = new DestroyableString(t);
				// for (; num > 0; num--)
				// data.remove(0);
			} else {// if (data == null || data.length() == 0) {
				data = new DestroyableString();
				long memCon = 3 * BLOCK_SIZE;
				moreData(memCon);
			}
		} else {
			// flip a coin, either shed it all or add some on
			if (data == null || data.length() == 0) {
				data = new DestroyableString();
				long memCon = 3 * BLOCK_SIZE;
				moreData(memCon);
			} else if (ExperimentParameterGeneration.getinstance().coinFlip()) {
				long memCon = getAdditionalPressureSize();
				// String d = data.toString();
				try {
					/* if (memCon > 0) */moreData(memCon);
				} catch (OutOfMemoryError ex) {
					try {
						String out = String.format(
								"Error: %s:%04x failed %04x mem allocation\n",
								user, completedSteps, memCon);
						JavaClientExperiments.getExp().stdout(out);
						moreData(BLOCK_SIZE);
					} catch (Exception ex1) {
						String out = String
								.format("Error: %s:%04x failed %04x, "
										+ ex1.toString(), user, completedSteps,
										memCon);
						JavaClientExperiments.getExp().stdout(out);
						// data = new DestroyableString(d);
					}
				} catch (Exception ex) {
					try {
						String out = String.format(
								"Error: %s:%04x failed %04x, " + ex.toString(),
								user, completedSteps, memCon);
						JavaClientExperiments.getExp().stdout(out);
						moreData(BLOCK_SIZE);
					} catch (Exception ex1) {
						String out = String
								.format("Error: %s:%04x failed %04x, "
										+ ex1.toString(), user, completedSteps,
										memCon);
						JavaClientExperiments.getExp().stdout(out);
					}
				}
			} else {
				sendDatagramPacket();
				if (wipe)
					data.destroy();
				data = new DestroyableString();
			}
		}
	}

	public void small_req() throws ClientProtocolException, IOException {
		HttpClient client = buildClient();
		HttpGet get = new HttpGet(getSmallUrl());
		HttpResponse response = client.execute(get);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response
				.getEntity().getContent()));
		String line = "";
		StringBuilder op = new StringBuilder();

		while ((line = rd.readLine()) != null) {
			op.append(line);
		}
		completedSteps++;
		client = null;
		JavaClientExperiments.getExp().incrementRequest();
		updateMemoryConsumption();
	}

	class PerformLogin extends TimerTask {
		public void run() {
			try {
				if (use_ssl_socket)
					login2();
				else
					login();
			} catch (IOException e) {
				synchronized (alive) {
					error = true;
				}
				e.printStackTrace();
				String out = String.format(
						"Error: %s:%04x failed login, " + e.toString(), user,
						completedSteps);
				JavaClientExperiments.getExp().stdout(out);
			}
			reportSize();
			schedule_next_task();
		}
	}

	class PerformMediumReq extends TimerTask {
		public void run() {
			try {
				synchronized (alive) {
					if (alive && use_ssl_socket)
						medium_req2();
					else if (alive)
						medium_req();
				}
			} catch (IOException e) {
				synchronized (alive) {
					error = true;
					e.printStackTrace();
					String out = String
							.format("Error: %s:%04x failed medium req, "
									+ e.toString(), user, completedSteps);
					JavaClientExperiments.getExp().stdout(out);

				}
			}
			reportSize();
			schedule_next_task();
		}
	}

	class PerformSmalleReq extends TimerTask {
		public void run() {
			try {
				synchronized (alive) {
					if (alive && use_ssl_socket)
						small_req2();
					else if (alive)
						small_req();
				}
			} catch (IOException e) {
				synchronized (alive) {
					error = true;
					String out = String.format(
							"Error: %s:%04x failed small req, " + e.toString(),
							user, completedSteps);
					JavaClientExperiments.getExp().stdout(out);
				}
			}
			reportSize();
			schedule_next_task();
		}
	}

	public String getLoginUrl() {
		return String.format("https://%s/%s", host, LOGIN_URL);
	}

	public String getSmallUrl() {
		return String.format("https://%s/%s", host, SMALL_URL);
	}

	public String getMedUrl() {
		return String.format("https://%s/%s", host, MED_URL);
	}

	Byte[] concat(Byte[]... arrays) {
		// Determine the length of the result array
		int totalLength = 0;
		for (int i = 0; i < arrays.length; i++) {
			totalLength += arrays[i].length;
		}

		// create the result array
		Byte[] result = new Byte[totalLength];

		// copy the source arrays into the result array
		int currentIndex = 0;
		for (int i = 0; i < arrays.length; i++) {
			System.arraycopy(arrays[i], 0, result, currentIndex,
					arrays[i].length);
			currentIndex += arrays[i].length;
		}

		return result;
	}

	Byte[] toObjects(byte[] bytesPrim) {
		Byte[] bytes = new Byte[bytesPrim.length];
		Arrays.setAll(bytes, n -> bytesPrim[n]);
		return bytes;
	}

	public Boolean isAlive() {
		return alive && nextStepTimer != null;
	}

	public void setAlive(Boolean alive) {
		synchronized (this.alive) {
			this.alive = alive;
		}
	}

	public Boolean getCompleted() {
		return completed;
	}

	public String getExpID() {
		return user.toString();
	}

	public void setShutdown() {
		shutdown = true;
	}

	public void setUseSSLSocket() {
		use_ssl_socket = true;

	}
	
	public void setSSLSocketNull() {
		set_ssl_socket_null = true;

	}


	public boolean getPause() {
		return pause;
	}


	public void setPause(boolean pause) {
		this.pause = pause;
	}
}
