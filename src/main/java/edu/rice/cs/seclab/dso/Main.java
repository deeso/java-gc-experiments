package edu.rice.cs.seclab.dso;

public class Main {
	public static void main(String[] args) throws Exception {
		String [] new_args = null;
		if (args.length > 0){
			new_args = new String[args.length-1];
			for (int i = 1; i < args.length; i++)
				new_args[i-1] = args[i];
		} else if (args.length == 0) {
			StringBuilder hsb = new StringBuilder();
			hsb.append("provide a command or class to call:\n");
			hsb.append("1] JavaClientExperiments-BC (bouncy castle)\n");
			hsb.append("2] JavaClientExperiments\n");
			//hsb.append("3] TlsClientWork\n");
			//hsb.append("4] TlsClientExample\n");
			throw new Exception(hsb.toString());
		}
		String cmd = args[0];
		if (cmd.equalsIgnoreCase("javaclientexperiments")) {
			JavaClientExperiments.main(new_args);
		} else if (cmd.equalsIgnoreCase("javaclientexperiments-bc")) {
			JavaClientExperiments.main_bc(new_args);
		} else if (cmd.equalsIgnoreCase("javaclientexperiments-socket")) {
			JavaClientExperiments.main_socket(new_args);
		} else if (cmd.equalsIgnoreCase("javaclientexperiments-socket-null")) {
			JavaClientExperiments.main_socket_null(new_args);
		} else { 
			throw new Exception(String.format("%s not a valid command!", cmd));
		}
	}
}
