package edu.rice.cs.seclab.dso;

import java.io.File;
import java.lang.management.ManagementFactory;
import java.net.URISyntaxException;

import com.sun.tools.attach.VirtualMachine;

public class ObjectSizeFetcherLoader { 
	// Needed to run the following command to get maven to include the file.
	//mvn deploy:deploy-file -Durl=file:///home/dso/.m2/repository/ -Dfile=/usr/lib/jvm/java-8-oracle/lib/tools.jar -DgroupId=com.sun -DartifactId=tools -Dpackaging=jar -Dversion=1.8

	    public static void loadAgent() throws URISyntaxException {
			File jarFilePath = null;
			try {
				jarFilePath = new File(ObjectSizeFetcherLoader.class
						.getProtectionDomain()
						.getCodeSource()
						.getLocation()
						.toURI()
						.getPath());
			} catch (URISyntaxException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				//throw e1;
			} catch (Exception e) {
				// TODO: handle exception
			}
			if (jarFilePath == null) return;
	        String nameOfRunningVM = ManagementFactory.getRuntimeMXBean().getName();
	        int p = nameOfRunningVM.indexOf('@');
	        String pid = nameOfRunningVM.substring(0, p);

	        try {
	            VirtualMachine vm = VirtualMachine.attach(pid);
	            vm.loadAgent(jarFilePath.toString(), "");
	            vm.detach();
	        } catch (Exception e) {
	            throw new RuntimeException(e);
	        }
	    }
}
