package edu.rice.cs.seclab.dso;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;

public class ExperimentParameterGeneration {
	private static ExperimentParameterGeneration expGen = null;
	SecureRandom rs = new SecureRandom();
	protected  ExperimentParameterGeneration() {}
	static public ExperimentParameterGeneration getinstance() {
		if (expGen == null)
			expGen = new ExperimentParameterGeneration();
		return expGen;
	}
	public String nextSessionId() {
		return new String(Base64.encodeInteger(getBigInt(240)));
	}
	
	public String randomString(int min_byte_num) {
		return RandomStringUtils.random(1000);
	}
	
	public String nextUserId() {
		return new String(Base64.encodeInteger(getBigInt(70)));
	}
	
	public String nextPasswordId() {
		return new String(Base64.encodeInteger(getBigInt(120)));
	}
	
	public BigInteger getBigInt(int bits) {
		return new BigInteger(bits, rs);
	}
	
	public byte[] randomBytes(int sz) {
		return rs.generateSeed(sz);
	}
	
	public int randomInt(int max) {
		
		int t = 0;
		do {
			t = rs.nextInt();
		} while (t == 0);
		
		return t % max;
	}
	public float randomFloat() {
		return rs.nextFloat();
	}
	
	public float randomFloat(float min, float max) {		
		float t = 0;
		do {
			t = rs.nextFloat();
		} while (t < min && t > max);
		return t;
	}
	public double randomDouble(double min, double max) {
		double t = 0;
		do {
			t = rs.nextDouble();
		} while (t < min && t > max);
		return t;
	}
	public Byte[] toObjects(byte[] bytesPrim) {
	    Byte[] bytes = new Byte[bytesPrim.length];
	    Arrays.setAll(bytes, n -> bytesPrim[n]);
	    return bytes;
	}
	
	public boolean coinFlip() {
		return rs.nextGaussian() > .5;
	}
	public float randomPercent() {
		float t = randomInt(100);
		if (t > 0) return t/100;
		return (float) 0.01;
	}
	public float randomPercent(float min, float max) {
		return randomFloat(min, max);
	}

	public double randomPercent(double min, double max) {
		return randomDouble(min, max);
	}

}
