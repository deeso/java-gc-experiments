package edu.rice.cs.seclab.dso;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

public class JavaClientExperiments {
	static public int MAX_EXPERIMENT_TIME = 3600;
	static public int EXPERIMENT_TIME = 240;
	static public int SCRIPT_EXPERIMENT_RUN_TIME = 60 * 5 + 10;
	static private JavaClientExperiments gJavaClientExp = null;
	static public int MAX_SESSION_LIFETIME = (int) (EXPERIMENT_TIME);
	static public int MAX_ALLOWED_REQ = 1000;
	static public int MAX_CONCURRENT_SESSIONS = 200;
	static public int USED_MEM = (int) (Runtime.getRuntime().totalMemory() - Runtime
			.getRuntime().freeMemory());
	// static public long MAX_MEMORY_2560 =
	// 2560L*1024*1024*2;//-USED_MEM;//Runtime.getRuntime().maxMemory()-USED_MEM;
	static public long MAX_MEMORY_16384 = 8192L * 1024 * 1024 * 2;// -USED_MEM;//Runtime.getRuntime().maxMemory()-USED_MEM;
	static public long MAX_MEMORY_8192 = 8192L * 1024 * 1024;// -USED_MEM;//Runtime.getRuntime().maxMemory()-USED_MEM;
	static public long MAX_MEMORY_4096 = 4096L * 1024 * 1024;// -USED_MEM;//Runtime.getRuntime().maxMemory()-USED_MEM;
	static public long MAX_MEMORY_3072 = 3072L * 1024 * 1024;// -USED_MEM;//Runtime.getRuntime().maxMemory()-USED_MEM;
	static public long MAX_MEMORY_2304 = 2304L * 1024 * 1024;// -USED_MEM;//Runtime.getRuntime().maxMemory()-USED_MEM;
	static public long MAX_MEMORY_2048 = 2048L * 1024 * 1024;// -USED_MEM;//Runtime.getRuntime().maxMemory()-USED_MEM;
	static public long MAX_MEMORY_1024 = 1024L * 1024 * 1024;// -USED_MEM;//Runtime.getRuntime().maxMemory()-USED_MEM;
	static public long MAX_MEMORY_512 = 512L * 1024 * 1024 - USED_MEM;// Runtime.getRuntime().maxMemory()-USED_MEM;
	static public long MAX_MEMORY = MAX_MEMORY_8192;
	
	static public int NUM_REQUESTS_GC_ITER_1G = 4500;
	static public int NUM_REQUESTS_GC_ITER_2G = 8500;
	static public int NUM_REQUESTS_GC_ITER_4G = 12500;
	static public int NUM_REQUESTS_GC_ITER_8G = 12500;
	static public int NUM_REQUESTS_GC_ITER_16G = 15500;
	
	static public int NUM_REQUESTS_GC = -1;
	
	static public long MEMORY_WATER_MARK = (long) (MAX_MEMORY);// (MAX_MEMORY*.80);
	static public long MIN_CUSHION = (long) (0.2 * MAX_MEMORY);
	public static Long numRequests = 0L;
	public static Long printEveryNumRequest = 500L;
	public static Long numAllowedRequests = 100000L;
	public static final String DATE_FORMAT = "yyyy.MM.dd_HH.mm.ss";
	public Boolean use_bouncy_castle = false;

	public boolean use_ssl_socket = false;

	void pauseAllExperiments() {
		synchronized (livingSessions) {
			for (ExperimentSession e : livingSessions.values()) {
				e.setPause(true);
			}
		}
	}

	void unpauseAllExperiments() {
		synchronized (livingSessions) {
			for (ExperimentSession e : livingSessions.values()) {
				e.setPause(false);
			}
		}
	}

	public boolean isExperimentsPause() {
		boolean x = true;
		synchronized (livingSessions) {
			for (ExperimentSession e : livingSessions.values()) {
				x = e.getPause();
				if (!x)
					break;
			}
		}
		return x;
	}

	public byte[][] attemptMemoryClear(long size) {
		long new_size = ((long) (size * .4)) >> 1;
		int i_size = (int) (new_size / Integer.MAX_VALUE);
		int i_rem = (int) (new_size % Integer.MAX_VALUE);
		int barray = i_size;
		byte[][] d = null;
		try {
			return new byte[barray][Integer.MAX_VALUE];
		} catch (OutOfMemoryError ex) {

		}
		return null;
	}

	public void incrementRequest() {
		String out = null;
		Boolean doTheGC = false;
		synchronized (numRequests) {
			numRequests++;
if (GC_START_TIME >= 10000 && numRequests == JavaClientExperiments.NUM_REQUESTS_GC) {
				pauseAllExperiments();
				long whattodo = GC_START_TIME - 10000;
				long num_gcs_todo = whattodo % 10, gc_spacing = whattodo
						- (whattodo % 10);
				
				if (GC_START_TIME >= 11000) {
					whattodo = GC_START_TIME - 10000;
					gc_spacing = whattodo;
				}
				try {
					Thread.sleep(1000L * 60);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				if (GC_START_TIME >= 11000) {
					try {
						//Thread.sleep(1 * gc_spacing);
						Runtime.getRuntime().gc(); // 1
						Thread.sleep(gc_spacing);
						Runtime.getRuntime().gc(); // 2
						Thread.sleep(gc_spacing);
						Runtime.getRuntime().gc(); // 3
						Thread.sleep(gc_spacing);
						Runtime.getRuntime().gc(); // 4
						Thread.sleep(gc_spacing);
					} catch (Exception e) {
					}
					long time_now = System.currentTimeMillis();
					out = String
								.format("Time:%08x-CompletedThreeGC:01-NumRequest:31137",
										time_now);
					JavaClientExperiments.getExp().stdout(out);
					try {
						Thread.sleep(10000000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
				
				long secs = 0;
				if (num_gcs_todo > 0) {
					Runtime.getRuntime().gc();
				}

				long max_secs = 60 * num_gcs_todo;

				Long time_now = System.currentTimeMillis();
				out = String.format("Time:%08x-NumRequest:%d", time_now,
						numRequests);
				JavaClientExperiments.getExp().stdout(out);
				long memSize = runtime.freeMemory();
				long iter = 0;
				while (true) {
					try {
						Thread.sleep(gc_spacing);
						if (num_gcs_todo > 0 && iter < num_gcs_todo) {
							Runtime.getRuntime().gc();

						}
						iter++;
						// if (secs % 60 == 0) {
						// byte [][] d = attemptMemoryClear(memSize);
						// time_now = System.currentTimeMillis();
						// if (d == null) {
						// out =
						// String.format("Time:%08x-ZeroizationSuccess:00",
						// time_now);
						// JavaClientExperiments.getExp().stdout(out);
						// } else {
						// out =
						// String.format("Time:%08x-ZeroizationSuccess:01",
						// time_now);
						// JavaClientExperiments.getExp().stdout(out);
						// }
						// d = null;
						// }
					} catch (Exception e) {
						break;
					}
					time_now = System.currentTimeMillis();
					if (iter >= num_gcs_todo) {
						out = String
								.format("Time:%08x-CompletedThreeGC:01-NumRequest:31137",
										time_now);
						JavaClientExperiments.getExp().stdout(out);
					}
				}
				// unpauseAllExperiments();
				while (true) {
					try {
						Thread.sleep(10000000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

			}
			switch(numRequests.intValue()) {
			case 5000:
				if (GC_START_TIME < 10000 && (MAX_MEMORY == MAX_MEMORY_1024 || MAX_MEMORY == MAX_MEMORY_512)) doTheGC = true;
				break;
			case 8000:
				if (GC_START_TIME < 10000 && (MAX_MEMORY == MAX_MEMORY_2048)) doTheGC = true;
				break;
			case 13000:
				if (GC_START_TIME < 10000 && (MAX_MEMORY == MAX_MEMORY_4096)) doTheGC = true;
				break;
			case 20000:
				if (GC_START_TIME < 10000 && (MAX_MEMORY == MAX_MEMORY_8192)) doTheGC = true;
				break;
			case 32000:
				if (GC_START_TIME < 10000 && (MAX_MEMORY == MAX_MEMORY_16384)) doTheGC = true;
				break;
			}
			
			if (doTheGC) {
				Runtime.getRuntime().gc();
				out = String.format(
						"Time:%08x-gc_called:%04x-next_gc_time:%04x",
						System.currentTimeMillis(), 1, 0);
				JavaClientExperiments.getExp().stdout(out);
				try {
					Thread.sleep(300);
				} catch (InterruptedException e) {
				}
			} 
			if (numRequests % printEveryNumRequest == 0) {
				Long time_now = System.currentTimeMillis();
				out = String.format("Time:%08x-NumRequest:%d", time_now,
						numRequests);
			}

		}

		if (out != null)
			JavaClientExperiments.getExp().stdout(out);

	}

	public Boolean useBouncyCastle() {
		return use_bouncy_castle;
	}

	public void setUseBouncyBastle(Boolean use_bouncy_castle) {
		this.use_bouncy_castle = use_bouncy_castle;
	}

	public Boolean dummy = false;

	static public enum USE_FACTOR {
		VHIGH, HIGH, MED, LOW, VLOW, RANDOM,
	};

	public double VHIGH_FACTOR = .9;
	public double HIGH_FACTOR = .8;
	public double MED_FACTOR = .4;
	public double LOW_FACTOR = .2;
	public double VLOW_FACTOR = .1;
	public double RANDOM_FACTOR = -1.0;

	private long sessLifetime = 0;
	private long allowedSessions = 0;
	private long allowedRequests = 0;

	private long memoryPerSession = 0;
	Runtime runtime = Runtime.getRuntime();
	private HashMap<String, ExperimentSession> livingSessions = new HashMap<String, ExperimentSession>();
	private String host;
	BufferedWriter UserAuthLogFile = null;
	private boolean destroy_pass = false;
	private String logfilename;

	private long maxAllowedMemoryAllocation = 0;

	long startTime = 0;

	Integer numSessionsCreated = 0;
	Thread monitorSessions = null;
	Timer signalTheEnd = new Timer();
	private Timer signalTheGC = new Timer();
	volatile private boolean keepRunning = true;
	protected STATE myCurrentState = STATE.EXP_NOT_STARTED;
	protected long endTime;
	public Object startCleaningUp;

	public static enum STATE {
		EXP_NOT_STARTED, EXP_RUNNING, EXP_COMPLETE
	};

	Boolean running = false;
	private boolean set_ssl_socket_null = false;

	public boolean isRunning() {
		synchronized (running) {
			return running;
		}
	}

	public void stdout(String msg) {
		synchronized (dummy) {
			System.out.println(msg);
		}
	}

	private void setRunning(boolean b) {
		synchronized (running) {
			running = false;
		}
		// notify();
	}

	public int getLiveSessions() {
		synchronized (livingSessions) {
			return livingSessions.size();
		}
	}

	public long getRemainingTime() {
		return endTime - System.currentTimeMillis();
	}

	public boolean canCreateNewSessInRemaingTime() {
		return true;// getSessLifetime() < getRemainingTime();
	}

	public boolean canCreateSessions() {
		return getLiveSessions() < getAllowedSessions()
				&& canCreateNewSessInRemaingTime();
	}

	public void start() {
		running = true;
		// kickstart the RunningTask, and continually monitor progress of THE
		// the children tasks, reap and handle accordingly
		// Thank you past me for being awesome at Java!! *\0/*
		// ref:
		// https://bitbucket.org/deeso/service/src/d2ab75eb4d403188f51e49609cc24628251e2d68/service/src/com/thecoverofnight/service/job/RunningTask.java?at=master#cl-644
		if (monitorSessions != null && monitorSessions.isAlive()) {
			return;
		}

		monitorSessions = new Thread(new Runnable() {

			@Override
			public void run() {
				running = true;
				long standardSleepTime = 1000;
				startTime = System.currentTimeMillis();
				endTime = startTime / 1000 + SCRIPT_EXPERIMENT_RUN_TIME;
				long eStartTime = startTime / 1000;
				long gstartTime = eStartTime + GC_START_TIME;
				long absEndTime = eStartTime + SCRIPT_EXPERIMENT_RUN_TIME;
				signalTheEnd.schedule(new SignalCleanup(), new Date(
						absEndTime * 1000));
				signalTheGC.schedule(new SignalGC(),
						new Date(gstartTime * 1000));
				String out = String.format("Time:%08x-next_gc_time:%08x",
						System.currentTimeMillis(), gstartTime * 1000);
				JavaClientExperiments.getExp().stdout(out);

				// signalTheGC.schedule(new SignalGC(), new
				// Date((eStartTime-45)*1000));
				while (keepRunning && myCurrentState != STATE.EXP_COMPLETE) {
					try {
						while (canCreateNewSession())
							createNewClientSession();
						synchronized (myCurrentState) {
							if (myCurrentState == STATE.EXP_COMPLETE)
								break;
						}
						Thread.sleep(standardSleepTime);
					} catch (InterruptedException e) {
						stdout("Interrupt has occurred, must need to start a client");
						if (myCurrentState == STATE.EXP_COMPLETE)
							break;
					} catch (Exception ex) {
						out = String.format("Error: JavaClientExperiments "
								+ ex.toString());
						JavaClientExperiments.getExp().stdout(out);
					}
				}
				stdout("Performing clean-up now.");
				performCleanup();
			}
		});
		monitorSessions.start();
	}

	public long getMaxMemoryConsumption() {
		return maxAllowedMemoryAllocation;
	}

	public long getSessLifetime() {
		if (USE_RANDOM_SESS_LIFETIME || sessLifetime <= 0) {
			double t = ExperimentParameterGeneration.getinstance()
					.randomDouble(LOW_FACTOR, HIGH_FACTOR);
			return doubleToLong(t * MAX_SESSION_LIFETIME);
		}
		return sessLifetime;
	}

	public void setSessLifetime(long sessLifetime) {
		this.sessLifetime = sessLifetime;
	}

	public double getMemoryPressureFactor() {
		if (livingSessions.size() == 0 && USE_RANDOM_MEM_PER_SESSION)
			return ExperimentParameterGeneration.getinstance().randomDouble(
					LOW_FACTOR, HIGH_FACTOR);
		else if (livingSessions.size() == 0)
			return memPressureFactor;
		else if (USE_RANDOM_MEM_PER_SESSION)
			return ExperimentParameterGeneration.getinstance().randomDouble(
					LOW_FACTOR, HIGH_FACTOR)
					/ livingSessions.size();
		return memPressureFactor / livingSessions.size();
	}

	public long getMemoryPerSession() {
		if (USE_RANDOM_MEM_PER_SESSION || memoryPerSession <= 0) {
			double t = ExperimentParameterGeneration.getinstance()
					.randomDouble(LOW_FACTOR, HIGH_FACTOR);
			return doubleToLong(t * MAX_MEMORY);
		}
		return memoryPerSession;
	}

	public long getAllowedSessions() {
		return allowedSessions;
	}

	public long getAllowedRequests() {
		if (USE_RANDOM_ALLOWED_SESS || allowedRequests <= 0) {
			double t = ExperimentParameterGeneration.getinstance()
					.randomDouble(LOW_FACTOR, HIGH_FACTOR);
			return doubleToLong(t * MAX_ALLOWED_REQ);
		}
		return allowedRequests;
	}

	public void expNotify() {
		synchronized (myCurrentState) {
			if (myCurrentState == STATE.EXP_RUNNING)
				monitorSessions.notify();
		}
	}

	public void handleCompleteSession(String sessionId) {
		synchronized (livingSessions) {
			if (livingSessions.containsKey(sessionId)) {
				livingSessions.remove(sessionId);
			}
		}
		expNotify();
	}

	public void handleFailedSession(String sessionId) {
		synchronized (livingSessions) {
			if (livingSessions.containsKey(sessionId)) {
				livingSessions.remove(sessionId);
			}
		}
		expNotify();
	}

	public void createNewClientSession() {
		ExperimentSession esess = new ExperimentSession(this);
		if (use_ssl_socket)
			esess.setUseSSLSocket();
		if (set_ssl_socket_null)
			esess.setSSLSocketNull();

		synchronized (livingSessions) {
			livingSessions.put(esess.getExpID(), esess);
		}
		esess.start();
		long time = System.currentTimeMillis();
		String out = String.format("Starting-Time:%08x-Session:%s", time,
				esess.getExpID());
		stdout(out);
	}

	public boolean isDestroy_pass() {
		return destroy_pass;
	}

	public void setDestroy_pass(boolean destroy_pass) {
		this.destroy_pass = destroy_pass;
	}

	public void performCleanup() {
		ArrayList<ExperimentSession> sessions = new ArrayList<ExperimentSession>();
		signalTheEnd.cancel();
		synchronized (livingSessions) {
			stdout("Shutting down the sessions");
			for (ExperimentSession exp : livingSessions.values()) {
				if (exp.isAlive()) {
					sessions.add(exp);
					exp.setShutdown();
				}
			}
		}

		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
		}
		setRunning(false);

	}

	public boolean USE_RANDOM_SESS_LIFETIME = false;
	public boolean USE_RANDOM_MEM_PER_SESSION = false;
	public boolean USE_RANDOM_ALLOWED_SESS = false;
	public boolean USE_RANDOM_CONC_SESSIONS = false;
	public boolean USE_RANDOM_ALLOWED_REQ = false;
	private double memPressureFactor;

	public JavaClientExperiments(String host, String location,
			USE_FACTOR memPressureFactor, USE_FACTOR sessLifetimeFactor,
			USE_FACTOR reqAllowedFactor, USE_FACTOR numsessFactor)
			throws IOException {
		this.memPressureFactor = getFactorValue(memPressureFactor);
		maxAllowedMemoryAllocation = doubleToLong(this.memPressureFactor
				* MEMORY_WATER_MARK);

		if (maxAllowedMemoryAllocation < 0.0) {
			USE_RANDOM_MEM_PER_SESSION = true;
		}
		sessLifetime = doubleToLong(getFactorValue(sessLifetimeFactor)
				* MAX_SESSION_LIFETIME);
		if (sessLifetime < 0.0) {
			USE_RANDOM_SESS_LIFETIME = true;
		}
		allowedRequests = doubleToLong(getFactorValue(reqAllowedFactor)
				* MAX_ALLOWED_REQ);
		if (allowedRequests < 0.0) {
			USE_RANDOM_ALLOWED_REQ = true;
		}
		allowedSessions = doubleToLong(getFactorValue(numsessFactor)
				* MAX_CONCURRENT_SESSIONS);
		if (allowedSessions < 0.0) {
			double temp = ExperimentParameterGeneration.getinstance()
					.randomDouble(LOW_FACTOR, HIGH_FACTOR);
			allowedSessions = doubleToLong(temp * MAX_CONCURRENT_SESSIONS);
		}
		memoryPerSession = calculateMemoryPerSession();
		this.host = host;
		this.logfilename = location;
		openLogFile();
		gJavaClientExp = this;
	}

	public JavaClientExperiments(String host, String location,
			String memPressureFactor, String sessLifetimeFactor,
			String reqAllowedFactor, String numsessFactor) throws IOException {
		this.memPressureFactor = getFactorValue(memPressureFactor);
		maxAllowedMemoryAllocation = doubleToLong(getFactorValue(memPressureFactor)
				* MEMORY_WATER_MARK);
		if (maxAllowedMemoryAllocation < 0.0) {
			USE_RANDOM_MEM_PER_SESSION = true;
		}
		sessLifetime = doubleToLong(getFactorValue(sessLifetimeFactor)
				* MAX_SESSION_LIFETIME);
		if (sessLifetime < 0.0) {
			USE_RANDOM_SESS_LIFETIME = true;
		}
		allowedRequests = doubleToLong(getFactorValue(reqAllowedFactor)
				* MAX_ALLOWED_REQ);
		if (allowedRequests < 0.0) {
			USE_RANDOM_ALLOWED_REQ = true;
		}
		allowedSessions = doubleToLong(getFactorValue(numsessFactor)
				* MAX_CONCURRENT_SESSIONS);
		if (allowedSessions < 0.0) {
			double temp = ExperimentParameterGeneration.getinstance()
					.randomDouble(LOW_FACTOR, HIGH_FACTOR);
			allowedSessions = doubleToLong(temp * MAX_CONCURRENT_SESSIONS);
		}
		memoryPerSession = calculateMemoryPerSession();
		this.host = host;
		this.logfilename = location;
		openLogFile();
		gJavaClientExp = this;
	}

	public long doubleToLong(double v) {
		return (new Double(v)).longValue();
	}

	public double getFactorValue(USE_FACTOR factor) {
		switch (factor) {
		case VHIGH:
			return VHIGH_FACTOR;
		case HIGH:
			return HIGH_FACTOR;
		case MED:
			return MED_FACTOR;
		case LOW:
			return LOW_FACTOR;
		case VLOW:
			return VLOW_FACTOR;
		case RANDOM:
			return RANDOM_FACTOR;
		}
		return VLOW_FACTOR;
	}

	public double getFactorValue(String factor_string) {
		USE_FACTOR factor = USE_FACTOR.LOW;
		if (factor_string.equalsIgnoreCase("VHIGH"))
			factor = USE_FACTOR.VHIGH;
		else if (factor_string.equalsIgnoreCase("HIGH"))
			factor = USE_FACTOR.HIGH;
		else if (factor_string.equalsIgnoreCase("MED"))
			factor = USE_FACTOR.MED;
		else if (factor_string.equalsIgnoreCase("LOW"))
			factor = USE_FACTOR.LOW;
		else if (factor_string.equalsIgnoreCase("VLOW"))
			factor = USE_FACTOR.VLOW;
		else if (factor_string.equalsIgnoreCase("RANDOM"))
			factor = USE_FACTOR.RANDOM;

		switch (factor) {
		case VHIGH:
			return VHIGH_FACTOR;
		case HIGH:
			return HIGH_FACTOR;
		case MED:
			return MED_FACTOR;
		case LOW:
			return LOW_FACTOR;
		case VLOW:
			return VLOW_FACTOR;
		case RANDOM:
			return RANDOM_FACTOR;
		}
		return VLOW_FACTOR;
	}

	public boolean isMemoryCommittedAllocAboveThreshhold() {
		long committed_mem = memoryPerSession * livingSessions.size();
		// String s =
		// String.format("TotMem(%d)-CommittedMem(%d) > MIN_CUSHION(%d) == %d",
		// Runtime.getRuntime().totalMemory(), committed_mem, MIN_CUSHION,
		// Runtime.getRuntime().totalMemory() - committed_mem > MIN_CUSHION ? 1
		// : 0
		// );
		// System.out.println(s);

		return Runtime.getRuntime().totalMemory() - committed_mem > MIN_CUSHION;
	}

	public boolean allowedToAllocate(long allocationSize) {
		// String s = String.format("FreeMem(%d) > MIN_CUSHION(%d) == %d",
		// Runtime.getRuntime().freeMemory(), MIN_CUSHION,
		// Runtime.getRuntime().freeMemory() > MIN_CUSHION ? 1 : 0
		// );
		// System.out.println(s);
		// boolean d = isMemoryConsumptionBelowThreshold() &&
		// isMemoryCommittedAllocAboveThreshhold();
		return isMemoryConsumptionBelowThreshold(allocationSize) &&
		// isMemoryCommittedAllocAboveThreshhold() &&
				Runtime.getRuntime().freeMemory() > MIN_CUSHION;
	}
	
	public boolean isMemoryConsumptionBelowThreshold(long allocationSize) {
		if (allocationSize > doubleToLong(((double)(MAX_MEMORY_512) *.8))) {
			return false;
		}
		return isMemoryConsumptionBelowThreshold();
		
	}

	public boolean isMemoryConsumptionBelowThreshold() {
//		String s =
//		String.format("TotMem(%d)-FreeMem(%d) > maxAllowedMemoryAllocation(%d) == %d",
//		Runtime.getRuntime().totalMemory(),
//		Runtime.getRuntime().freeMemory(),
//		maxAllowedMemoryAllocation,
//		Runtime.getRuntime().totalMemory() -
//		Runtime.getRuntime().freeMemory() > maxAllowedMemoryAllocation ? 1 :
//		0
//		);
//		System.out.println(s);
		return Runtime.getRuntime().totalMemory()
				- Runtime.getRuntime().freeMemory() < maxAllowedMemoryAllocation;
	}

	public long calculateMemoryPerSession() {
		if (USE_RANDOM_MEM_PER_SESSION || memoryPerSession <= 0)
			return getMemoryPerSession();
		return maxAllowedMemoryAllocation / allowedSessions;
	}

	public boolean canCreateNewSession() {
		ArrayList<String> deadSessions = new ArrayList<String>();

		synchronized (livingSessions) {
			for (String s : livingSessions.keySet()) {
				ExperimentSession exp = livingSessions.get(s);
				if (!exp.isAlive())
					deadSessions.add(s);
			}

			for (String s : deadSessions) {
				if (livingSessions.containsKey(s))
					livingSessions.remove(s);
			}

			boolean memPred = true, sessPred = false;
			/*
			 * if ((USE_RANDOM_MEM_PER_SESSION || memoryPerSession <= 0)||
			 * (USE_RANDOM_ALLOWED_SESS || allowedSessions <= 0)) memPred =
			 * MEMORY_WATER_MARK > (Runtime.getRuntime().totalMemory() -
			 * Runtime.getRuntime().freeMemory()); else memPred =
			 * maxMemoryConsumption > (Runtime.getRuntime().totalMemory() -
			 * Runtime.getRuntime().freeMemory());
			 */
			if (USE_RANDOM_ALLOWED_SESS || allowedSessions <= 0)
				sessPred = livingSessions.size() < MAX_CONCURRENT_SESSIONS;
			else
				sessPred = livingSessions.size() < allowedSessions;
			return sessPred && memPred;

		}
	}

	public String getRemoteHost() {
		return host;
	}

	private String generateName() {
		// SimpleDateFormat sdfDate = new
		// SimpleDateFormat(DATE_FORMAT);//dd/MM/yyyy
		// Date now = new Date();
		// stdout(logfilename);
		Path path = null;
		path = Paths.get(logfilename);
		return path.toString();
	}

	private void openLogFile() throws IOException {
		String name = generateName();
		UserAuthLogFile = new BufferedWriter(new FileWriter(name, true));
	}

	public void logUserAuth(String time, String user, String pass, String sess)
			throws IOException {
		synchronized (UserAuthLogFile) {
			UserAuthLogFile.write(String.format("%s,%s,%s,%s\n", time, user,
					pass, sess));
			UserAuthLogFile.flush();
		}
	}

	class SignalCleanup extends TimerTask {
		public void run() {
			synchronized (myCurrentState) {
				myCurrentState = STATE.EXP_COMPLETE;
			}
		}
	}

	static int GC_CALLS = 0;
	static long GC_SCHEDULE_TIME = 60;
	static long GC_START_TIME = 600 - 30;

	class SignalGC extends TimerTask {
		public void run() {

			long _startTime = System.currentTimeMillis();
			// long _endTime = _startTime/1000 + EXPERIMENT_TIME;
			if (GC_SCHEDULE_TIME < 0) {
				pauseAllExperiments();
			}
			Runtime.getRuntime().gc();
			GC_CALLS++;
			String out = String.format(
					"Time:%08x-gc_called:%04x-next_gc_time:%04x",
					System.currentTimeMillis(), GC_CALLS, _startTime
							+ (GC_SCHEDULE_TIME * 1000));
			if (GC_SCHEDULE_TIME == -1) {
				out = String
						.format("Time:%08x-gc_paused_all_experiments:01-next_gc_time:00000000",
								System.currentTimeMillis(), GC_CALLS);
			} else if (GC_SCHEDULE_TIME < -1) {
				long sleep_time = Math.abs(GC_SCHEDULE_TIME);
				out = String.format(
						"Time:%08x-gc_called:%04x-next_gc_time:%04x",
						System.currentTimeMillis(), GC_CALLS, _startTime
								+ ((sleep_time + GC_SCHEDULE_TIME) * 1000));
				signalTheGC.schedule(new SignalGC(), new Date(_startTime
						+ ((sleep_time + GC_SCHEDULE_TIME) * 1000)));

			} else if (GC_SCHEDULE_TIME > 0) {
				signalTheGC.schedule(new SignalGC(), new Date(_startTime
						+ (GC_SCHEDULE_TIME * 1000)));
			}
			JavaClientExperiments.getExp().stdout(out);
			if (GC_SCHEDULE_TIME < -1) {
				long sleep_time = Math.abs(GC_SCHEDULE_TIME);
				long secs = 0;
				try {
					Thread.sleep(sleep_time * 1000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				unpauseAllExperiments();
			} else if (GC_SCHEDULE_TIME == -1) {
				long secs = 0;
				while (true) {
					try {
						Thread.sleep(1 * 1000);
						secs++;
						if (secs % 60 == 0) {
							Runtime.getRuntime().gc();
						}
					} catch (Exception e) {
						break;
					}
				}
				unpauseAllExperiments();
			}
		}
	}

	public static JavaClientExperiments getExp() {
		return gJavaClientExp;
	}

	public static void setMaxMemoryAndNumRequests(String mem_size) {
		long memSize = 8192L;
		int iMemSize = (int) memSize;
		try {
			memSize = Long.parseLong(mem_size);
			iMemSize = (int) memSize;
		} catch (Exception ex) {
		}
		switch (iMemSize) {
		case 1024: JavaClientExperiments.NUM_REQUESTS_GC = JavaClientExperiments.NUM_REQUESTS_GC_ITER_1G; break;
		case 2048: JavaClientExperiments.NUM_REQUESTS_GC = JavaClientExperiments.NUM_REQUESTS_GC_ITER_2G; break;
		case 4096: JavaClientExperiments.NUM_REQUESTS_GC = JavaClientExperiments.NUM_REQUESTS_GC_ITER_4G; break;
		case 8192: JavaClientExperiments.NUM_REQUESTS_GC = JavaClientExperiments.NUM_REQUESTS_GC_ITER_8G; break;
		case 16384: JavaClientExperiments.NUM_REQUESTS_GC = JavaClientExperiments.NUM_REQUESTS_GC_ITER_16G; break;
		default: break;		
		}
		switch (iMemSize) {
		case 512: JavaClientExperiments.MAX_MEMORY = JavaClientExperiments.MAX_MEMORY_512; break;
		case 1024: JavaClientExperiments.MAX_MEMORY = JavaClientExperiments.MAX_MEMORY_1024; break;
		case 2048: JavaClientExperiments.MAX_MEMORY = JavaClientExperiments.MAX_MEMORY_2048; break;
		case 2304: JavaClientExperiments.MAX_MEMORY = JavaClientExperiments.MAX_MEMORY_2304; break;
		case 3072: JavaClientExperiments.MAX_MEMORY = JavaClientExperiments.MAX_MEMORY_3072; break;
		case 4096: JavaClientExperiments.MAX_MEMORY = JavaClientExperiments.MAX_MEMORY_4096; break;
		case 8192: JavaClientExperiments.MAX_MEMORY = JavaClientExperiments.MAX_MEMORY_8192; break;
		case 16384: JavaClientExperiments.MAX_MEMORY = JavaClientExperiments.MAX_MEMORY_16384; break;
		default: JavaClientExperiments.MAX_MEMORY = JavaClientExperiments.MAX_MEMORY_8192; break;
//		case 512: JavaClientExperiments.MAX_MEMORY = 512;break;
//		case 1024: JavaClientExperiments.MAX_MEMORY = 1024; break;
//		case 2048: JavaClientExperiments.MAX_MEMORY = 2048; break;
//		case 2304: JavaClientExperiments.MAX_MEMORY = 2304; break;
//		case 3072: JavaClientExperiments.MAX_MEMORY = 3072; break;
//		case 4096: JavaClientExperiments.MAX_MEMORY = 4096; break;
//		case 8192: JavaClientExperiments.MAX_MEMORY = 8192; break;
//		case 16384: JavaClientExperiments.MAX_MEMORY = 16384; break;
//		default: JavaClientExperiments.MAX_MEMORY = 8192; break;
		
		}
		JavaClientExperiments.MEMORY_WATER_MARK = (long) (JavaClientExperiments.MAX_MEMORY);
		JavaClientExperiments.MIN_CUSHION = (long) (0.2 * JavaClientExperiments.MAX_MEMORY);


		
	}
	
	public static void main(String[] args) {
		JavaClientExperiments jclientExp = null;
		try {
			if (args.length < 10) {
				StringBuilder hsb = new StringBuilder();
				hsb.append(
						"JavaClientExperiment <memsize> <run_time_seconds> <gc_start_time> <gc_spacing_time> <host> <log_dir_location> <log_filename> <memory_pressure_factor>")
						.append(" <lifetime_factor>, <request_factor>, <concurrent_session_factor> [destroy_password]\n");
				hsb.append("Allowed factor values are : VLOW, LOW, MED, HIGH, VHIGH\n");
				System.err.println(hsb.toString());
				System.err.println("====ERROR====");
				return;
			}

			String mem_size = args[0], run_time = args[1], gc_start_time = args[2], gc_spacing_time = args[3], host = args[4], log_filename = args[5], memory_pressure_factor = args[6], lifetime_factor = args[7], request_factor = args[8], concurrent_session_factor = args[9];
			boolean destroy_password = args.length < 10 ? false : Boolean
					.parseBoolean(args[10]);
			JavaClientExperiments.setMaxMemoryAndNumRequests(mem_size);

			try {
				Integer runtime = Integer.parseInt(run_time);
				JavaClientExperiments.SCRIPT_EXPERIMENT_RUN_TIME = runtime;
			} catch (Exception ex) {
				JavaClientExperiments.SCRIPT_EXPERIMENT_RUN_TIME = 300;
			}

			try {
				Integer gcStartTime = Integer.parseInt(gc_start_time);
				JavaClientExperiments.GC_START_TIME = gcStartTime;
			} catch (Exception ex) {
				JavaClientExperiments.GC_START_TIME = 270;
			}

			try {
				Integer gcSpacingTime = Integer.parseInt(gc_spacing_time);
				JavaClientExperiments.GC_SCHEDULE_TIME = gcSpacingTime;
			} catch (Exception ex) {
				JavaClientExperiments.GC_START_TIME = 60;
			}

			try {
				StringBuilder sbt = new StringBuilder();
				String s = new String();
				int idx = 0;
				while (idx < 4096) {
					idx++;
					s = s + "A";
				}
				while (true) {
					sbt.append(s);
				}

			} catch (OutOfMemoryError oomex) {

			}

			try {
				jclientExp = new JavaClientExperiments(host, log_filename,
						memory_pressure_factor, lifetime_factor,
						request_factor, concurrent_session_factor);
				jclientExp.setDestroy_pass(destroy_password);
				// System.out.println(String.format("Memory Size: 0x%08x, mem/session: 0x%08x",
				// jclientExp.maxMemoryConsumption,
				// jclientExp.calculateMemoryPerSession()));
				ObjectSizeFetcherLoader.loadAgent();
			} catch (IOException ex) {
				// ex.printStackTrace();
				jclientExp.stdout("Error: " + ex.toString());
				System.out.println("====ERROR====");
				return;
			} catch (NullPointerException ex) {
				// ex.printStackTrace();
				jclientExp.stdout("Error: " + ex.toString());
				System.out.println("====ERROR====");
				return;
			}
			jclientExp.start();
			while (jclientExp.isRunning()) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					jclientExp.stdout("Error: " + e.toString());
				} catch (OutOfMemoryError e) {
					jclientExp.stdout("Error: " + e.toString());
				} catch (Exception e) {
					jclientExp.stdout("Error: " + e.toString());
					// TODO Auto-generated catch block
				}
			}
			// StringBuilder sb = new StringBuilder();
			// sb.append("User: ").append(user).append(" Password: ").append(pass).append("\n");
			// stdout(sb.toString());
			jclientExp.stdout("====DONE====");
			try {
				Thread.sleep(1000 * 60);
			} catch (InterruptedException e) {
				jclientExp.stdout("Error: " + e.toString());
			} catch (OutOfMemoryError e) {
				jclientExp.stdout("Error: " + e.toString());
			} catch (Exception e) {
				jclientExp.stdout("Error: " + e.toString());
				// TODO Auto-generated catch block
			}
			return;
		} catch (Exception e) {
			jclientExp.stdout("Error: " + e.toString());
			// TODO Auto-generated catch block
		} finally {
			if (jclientExp != null)
				jclientExp.stdout("====DONE====");
		}
	}

	public static void main_bc(String[] args) {
		JavaClientExperiments jclientExp = null;
		try {
			if (args.length < 10) {
				StringBuilder hsb = new StringBuilder();
				hsb.append(
						"JavaClientExperiment <memsize> <run_time_seconds> <gc_start_time> <gc_spacing_time> <host> <log_dir_location> <log_filename> <memory_pressure_factor>")
						.append(" <lifetime_factor>, <request_factor>, <concurrent_session_factor> [destroy_password]\n");
				hsb.append("Allowed factor values are : VLOW, LOW, MED, HIGH, VHIGH\n");
				System.err.println(hsb.toString());
				System.err.println("====ERROR====");
				return;
			}

			String mem_size = args[0], run_time = args[1], gc_start_time = args[2], gc_spacing_time = args[3], host = args[4], log_filename = args[5], memory_pressure_factor = args[6], lifetime_factor = args[7], request_factor = args[8], concurrent_session_factor = args[9];
			boolean destroy_password = args.length < 10 ? false : Boolean
					.parseBoolean(args[10]);
			JavaClientExperiments.setMaxMemoryAndNumRequests(mem_size);


			try {
				Integer runtime = Integer.parseInt(run_time);
				JavaClientExperiments.SCRIPT_EXPERIMENT_RUN_TIME = runtime;
			} catch (Exception ex) {
				JavaClientExperiments.SCRIPT_EXPERIMENT_RUN_TIME = 300;
			}

			try {
				Integer gcStartTime = Integer.parseInt(gc_start_time);
				JavaClientExperiments.GC_START_TIME = gcStartTime;
			} catch (Exception ex) {
				JavaClientExperiments.GC_START_TIME = 270;
			}

			try {
				Integer gcSpacingTime = Integer.parseInt(gc_spacing_time);
				JavaClientExperiments.GC_SCHEDULE_TIME = gcSpacingTime;
			} catch (Exception ex) {
				JavaClientExperiments.GC_START_TIME = 60;
			}
			try {
				StringBuilder sbt = new StringBuilder();
				String s = new String();
				int idx = 0;
				while (idx < 4096) {
					idx++;
					s = s + "A";
				}
				while (true) {
					sbt.append(s);
				}

			} catch (OutOfMemoryError oomex) {

			}
			try {
				jclientExp = new JavaClientExperiments(host, log_filename,
						memory_pressure_factor, lifetime_factor,
						request_factor, concurrent_session_factor);
				jclientExp.setDestroy_pass(destroy_password);
				jclientExp.setUseBouncyBastle(true);
				// System.out.println(String.format("Memory Size: 0x%08x, mem/session: 0x%08x",
				// jclientExp.maxMemoryConsumption,
				// jclientExp.calculateMemoryPerSession()));
				ObjectSizeFetcherLoader.loadAgent();
			} catch (IOException ex) {
				// ex.printStackTrace();
				jclientExp.stdout("Error: " + ex.toString());
				System.out.println("====ERROR====");
				return;
			} catch (NullPointerException ex) {
				// ex.printStackTrace();
				jclientExp.stdout("Error: " + ex.toString());
				System.out.println("====ERROR====");
				return;
			}
			jclientExp.start();
			while (jclientExp.isRunning()) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					jclientExp.stdout("Error: " + e.toString());
				} catch (OutOfMemoryError e) {
					jclientExp.stdout("Error: " + e.toString());
				} catch (Exception e) {
					jclientExp.stdout("Error: " + e.toString());
					// TODO Auto-generated catch block
				}
			}
			// StringBuilder sb = new StringBuilder();
			// sb.append("User: ").append(user).append(" Password: ").append(pass).append("\n");
			// stdout(sb.toString());
			jclientExp.stdout("====DONE====");
			try {
				Thread.sleep(1000 * 60);
			} catch (InterruptedException e) {
				jclientExp.stdout("Error: " + e.toString());
			} catch (OutOfMemoryError e) {
				jclientExp.stdout("Error: " + e.toString());
			} catch (Exception e) {
				jclientExp.stdout("Error: " + e.toString());
				// TODO Auto-generated catch block
			}
			return;
		} catch (Exception e) {
			jclientExp.stdout("Error: " + e.toString());
			// TODO Auto-generated catch block
		} finally {
			if (jclientExp != null)
				jclientExp.stdout("====DONE====");
		}
	}

	public static void main_socket(String[] args) {
		JavaClientExperiments jclientExp = null;
		try {
			if (args.length < 10) {
				StringBuilder hsb = new StringBuilder();
				hsb.append(
						"JavaClientExperiment <memsize> <run_time_seconds> <gc_start_time> <gc_spacing_time> <host> <log_dir_location> <log_filename> <memory_pressure_factor>")
						.append(" <lifetime_factor>, <request_factor>, <concurrent_session_factor> [destroy_password]\n");
				hsb.append("Allowed factor values are : VLOW, LOW, MED, HIGH, VHIGH\n");
				System.err.println(hsb.toString());
				System.err.println("====ERROR====");
				return;
			}

			String mem_size = args[0], run_time = args[1], gc_start_time = args[2], gc_spacing_time = args[3], host = args[4], log_filename = args[5], memory_pressure_factor = args[6], lifetime_factor = args[7], request_factor = args[8], concurrent_session_factor = args[9];
			boolean destroy_password = args.length < 10 ? false : Boolean
					.parseBoolean(args[10]);
			JavaClientExperiments.setMaxMemoryAndNumRequests(mem_size);


			try {
				Integer runtime = Integer.parseInt(run_time);
				JavaClientExperiments.SCRIPT_EXPERIMENT_RUN_TIME = runtime;
			} catch (Exception ex) {
				JavaClientExperiments.SCRIPT_EXPERIMENT_RUN_TIME = 300;
			}

			try {
				Integer gcStartTime = Integer.parseInt(gc_start_time);
				JavaClientExperiments.GC_START_TIME = gcStartTime;
			} catch (Exception ex) {
				JavaClientExperiments.GC_START_TIME = 270;
			}

			try {
				Integer gcSpacingTime = Integer.parseInt(gc_spacing_time);
				JavaClientExperiments.GC_SCHEDULE_TIME = gcSpacingTime;
			} catch (Exception ex) {
				JavaClientExperiments.GC_START_TIME = 60;
			}
			try {
				StringBuilder sbt = new StringBuilder();
				String s = new String();
				int idx = 0;
				while (idx < 4096) {
					idx++;
					s = s + "A";
				}
				while (true) {
					sbt.append(s);
				}

			} catch (OutOfMemoryError oomex) {

			}
			try {
				jclientExp = new JavaClientExperiments(host, log_filename,
						memory_pressure_factor, lifetime_factor,
						request_factor, concurrent_session_factor);
				jclientExp.setUseSSLSockets();
				jclientExp.setDestroy_pass(destroy_password);
				jclientExp.setUseBouncyBastle(true);
				// System.out.println(String.format("Memory Size: 0x%08x, mem/session: 0x%08x",
				// jclientExp.maxMemoryConsumption,
				// jclientExp.calculateMemoryPerSession()));
				ObjectSizeFetcherLoader.loadAgent();
			} catch (IOException ex) {
				// ex.printStackTrace();
				jclientExp.stdout("Error: " + ex.toString());
				System.out.println("====ERROR====");
				return;
			} catch (NullPointerException ex) {
				// ex.printStackTrace();
				jclientExp.stdout("Error: " + ex.toString());
				System.out.println("====ERROR====");
				return;
			}
			jclientExp.start();
			while (jclientExp.isRunning()) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					jclientExp.stdout("Error: " + e.toString());
				} catch (OutOfMemoryError e) {
					jclientExp.stdout("Error: " + e.toString());
				} catch (Exception e) {
					jclientExp.stdout("Error: " + e.toString());
					// TODO Auto-generated catch block
				}
			}
			// StringBuilder sb = new StringBuilder();
			// sb.append("User: ").append(user).append(" Password: ").append(pass).append("\n");
			// stdout(sb.toString());
			jclientExp.stdout("====DONE====");
			try {
				Thread.sleep(1000 * 60);
			} catch (InterruptedException e) {
				jclientExp.stdout("Error: " + e.toString());
			} catch (OutOfMemoryError e) {
				jclientExp.stdout("Error: " + e.toString());
			} catch (Exception e) {
				jclientExp.stdout("Error: " + e.toString());
				// TODO Auto-generated catch block
			}
			return;
		} catch (Exception e) {
			jclientExp.stdout("Error: " + e.toString());
			// TODO Auto-generated catch block
		} finally {
			if (jclientExp != null)
				jclientExp.stdout("====DONE====");
		}
	}

	public static void main_socket_null(String[] args) {
		JavaClientExperiments jclientExp = null;
		try {
			if (args.length < 10) {
				StringBuilder hsb = new StringBuilder();
				hsb.append(
						"JavaClientExperiment <memsize> <run_time_seconds> <gc_start_time> <gc_spacing_time> <host> <log_dir_location> <log_filename> <memory_pressure_factor>")
						.append(" <lifetime_factor>, <request_factor>, <concurrent_session_factor> [destroy_password]\n");
				hsb.append("Allowed factor values are : VLOW, LOW, MED, HIGH, VHIGH\n");
				System.err.println(hsb.toString());
				System.err.println("====ERROR====");
				return;
			}

			String mem_size = args[0], run_time = args[1], gc_start_time = args[2], gc_spacing_time = args[3], host = args[4], log_filename = args[5], memory_pressure_factor = args[6], lifetime_factor = args[7], request_factor = args[8], concurrent_session_factor = args[9];
			boolean destroy_password = args.length < 10 ? false : Boolean
					.parseBoolean(args[10]);

			JavaClientExperiments.setMaxMemoryAndNumRequests(mem_size);


			try {
				Integer runtime = Integer.parseInt(run_time);
				JavaClientExperiments.SCRIPT_EXPERIMENT_RUN_TIME = runtime;
			} catch (Exception ex) {
				JavaClientExperiments.SCRIPT_EXPERIMENT_RUN_TIME = 300;
			}

			try {
				Integer gcStartTime = Integer.parseInt(gc_start_time);
				JavaClientExperiments.GC_START_TIME = gcStartTime;
			} catch (Exception ex) {
				JavaClientExperiments.GC_START_TIME = 270;
			}

			try {
				Integer gcSpacingTime = Integer.parseInt(gc_spacing_time);
				JavaClientExperiments.GC_SCHEDULE_TIME = gcSpacingTime;
			} catch (Exception ex) {
				JavaClientExperiments.GC_START_TIME = 60;
			}
			try {
				StringBuilder sbt = new StringBuilder();
				String s = new String();
				int idx = 0;
				while (idx < 4096) {
					idx++;
					s = s + "A";
				}
				while (true) {
					sbt.append(s);
				}

			} catch (OutOfMemoryError oomex) {

			}
			try {
				jclientExp = new JavaClientExperiments(host, log_filename,
						memory_pressure_factor, lifetime_factor,
						request_factor, concurrent_session_factor);
				jclientExp.setUseSSLSockets();
				jclientExp.setSSLSocketNull();
				jclientExp.setDestroy_pass(destroy_password);
				jclientExp.setUseBouncyBastle(true);
				// System.out.println(String.format("Memory Size: 0x%08x, mem/session: 0x%08x",
				// jclientExp.maxMemoryConsumption,
				// jclientExp.calculateMemoryPerSession()));
				ObjectSizeFetcherLoader.loadAgent();
			} catch (IOException ex) {
				// ex.printStackTrace();
				jclientExp.stdout("Error: " + ex.toString());
				System.out.println("====ERROR====");
				return;
			} catch (NullPointerException ex) {
				// ex.printStackTrace();
				jclientExp.stdout("Error: " + ex.toString());
				System.out.println("====ERROR====");
				return;
			}
			jclientExp.start();
			while (jclientExp.isRunning()) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					jclientExp.stdout("Error: " + e.toString());
				} catch (OutOfMemoryError e) {
					jclientExp.stdout("Error: " + e.toString());
				} catch (Exception e) {
					jclientExp.stdout("Error: " + e.toString());
					// TODO Auto-generated catch block
				}
			}
			// StringBuilder sb = new StringBuilder();
			// sb.append("User: ").append(user).append(" Password: ").append(pass).append("\n");
			// stdout(sb.toString());
			jclientExp.stdout("====DONE====");
			try {
				Thread.sleep(1000 * 60);
			} catch (InterruptedException e) {
				jclientExp.stdout("Error: " + e.toString());
			} catch (OutOfMemoryError e) {
				jclientExp.stdout("Error: " + e.toString());
			} catch (Exception e) {
				jclientExp.stdout("Error: " + e.toString());
				// TODO Auto-generated catch block
			}
			return;
		} catch (Exception e) {
			jclientExp.stdout("Error: " + e.toString());
			// TODO Auto-generated catch block
		} finally {
			if (jclientExp != null)
				jclientExp.stdout("====DONE====");
		}
	}

	private void setUseSSLSockets() {
		use_ssl_socket = true;
	}

	private void setSSLSocketNull() {
		set_ssl_socket_null = true;
	}

}
